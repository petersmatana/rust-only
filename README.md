# Install Rust on Fedora

Install Rust and package manager Cargo.

```
dnf install rust cargo
```

# Study materials

https://doc.rust-lang.org/stable/rust-by-example/

https://doc.rust-lang.org/stable/book/

# Ownership

## The Stack (zásobník)

Last in, first out. When element is *push* into stack, the *pop* operation will return this element.

## The Heap (halda)

Autobalanced binary tree (Which can be implemented with array).

In Rust when data is inserted into the Heap it is requesting certain amount of space. 
