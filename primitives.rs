fn main() {
    primitives();

    println!();

    let origin_tuple: (i32, bool) = (123, true);
    let x = reverse_tuple(origin_tuple);
    println!("bool = {}, number = {}", x.0, x.1);

    println!();

    play_with_tuples();
}

fn play_with_tuples() {
    let long_tuple: (i32, (u32, bool), (char, i32)) = (1, (1, true), ('a', 1));

    println!("whole tuple = {:?}", long_tuple);
    println!("bool = {}", long_tuple.1.1);
    println!("first tuple = {:?}", long_tuple.1);
    println!("second tuple = {:?}", long_tuple.2);
}

fn reverse_tuple(pair: (i32, bool)) -> (bool, i32) {
    (pair.1, pair.0)
}

fn primitives() {
    let i8var: i8 = -1;
    let i16var: i16 = -1;
    let i32var: i32 = -1;
    let i64var: i64 = -1;
    let i128var: i128 = -1;

    println!("i8 = {} \ni16 = {} \ni32 = {} \ni64 = {} \ni128 = {}", i8var, i16var, i32var, i64var, i128var);

    println!();

    let u8var: u8 = 1;
    let u16var: u16 = 1;
    let u32var: u32 = 1;
    let u64var: u64 = 1;
    let u128var: u128 = 1;

    println!("u8 = {} \nu16 = {} \nu32 = {} \nu64 = {} \nu128 = {}", u8var, u16var, u32var, u64var, u128var);

    println!();

    let f32var: f32 = 1.0;
    let f64var: f64 = 1.0;
    println!("f32 = {}\nf64 = {}", f32var, f64var);

    println!();

    let charvar: char = 'a';
    let boolvar: bool = true;
    println!("char = {}\nbool = {}", charvar, boolvar);

    println!();

    let tuplevar = (1,2,3,4);
    let arrayvar = [1,2,3,4];
    println!("tuple = {:?}\narray = {:?}", tuplevar, arrayvar);
    println!("first tuple element = {}", tuplevar.0);
}
