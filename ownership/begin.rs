fn main() {
    // Order of study:
    
    // 1. how scope works
    scope();

    // 2. how allocate variable on stack
    owner();

    // 3. how allocate variable on heap
    string();

    // 4. how copy from stack
    copy_from_stack();

    // 5. move in heap
    copy_from_heap();

    // 6. use clone fn
    use_clone();
}

fn scope() {
    let a = "other scope";
    {
        let s = "hello";
        println!("in the scope = {}", s);

        // s is droped here sice execution go
        // out of this scope
    }
    println!("out of the scope = {}", a);

    // same here for a. when execution go
    // out of this scope
}

fn owner() {
    // Each Rust value has it's owner.
    // Here 5 is owned by x.
    //
    // In this case, blob of memory is
    // allocated on stack.
    let x = 5;

    println!("{}", x);
}

fn string() {
    // At compile time, I do not know
    // how much memory do I need to
    // allocate.
    //
    // In this case, blob of memory is
    // allocated on heap.
    let mut s = String::from("hello");

    println!("{}", s);

    s.push_str(" world");
    
    println!("{}", s);

    // Of course s is free here.
}

fn copy_from_stack() {
    // If value is stored in stack, it
    // is easy to copy it.

    let x = 5;
    let y = x; // copy value x into y

    println!("x={},y={}",x,y);
}

fn copy_from_heap() {
    // If value is store in heap, it
    // is make copy of pointer, not a value.

    let a = String::from("move");
    let b = a; // move the value a into b
    // Because of move a into b, a here
    // is no more exist.


    // This will throw an error because
    // a has been moved or ownership
    // has been transferred.
    //
    // In Rust terminology "move" means
    // that ownership of a is
    // transferred to b.
    // println!("{}", a);

    println!("{}", b);
}

fn use_clone() {
    let a = String::from("use clone");
    let b = a.clone();

    println!("{}", a);
    println!("{}", b);
}
