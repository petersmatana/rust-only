fn main() {
    let mut s1 = String::from("string");
    
    println!("value of s1 = {}", s1);
    change_string(&mut s1);
    println!("again print s1 = {}", s1);

    let s2 = &mut s1;
    println!("value of s2 = {}", s2);
    println!("again print s1 = {}", s1);

    let s3 = &mut s1;
    println!("value of s3 = {}", &s3);
    println!("again print s1 = {}", &s1);
    // Can not do this because s2 is no longer
    // owner of the s1?
    // println!("again print s2 = {}", &mut s2);
}

fn change_string(s: &mut String) {
    s.push_str(" - suffix");
}
